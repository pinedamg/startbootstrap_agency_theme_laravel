<?php
/**
 * Created by PhpStorm.
 * User: mpineda
 * Date: 04/01/17
 * Time: 14:58
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class MainController extends Controller
{
    public function home()
    {
        return view('layouts.main');
    }
}