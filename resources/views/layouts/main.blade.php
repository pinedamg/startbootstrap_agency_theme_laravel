<!DOCTYPE html>
<html lang="en">

@include('layouts.partials.head')

<body id="page-top" class="index">
@include('layouts.partials.nav')
@include('layouts.partials.header')

@include('sections.services')
@include('sections.portfolio')
@include('sections.about')
@include('sections.team')
@include('sections.contact')

@include('layouts.partials.footer')
@yield('footer.after')

@include('layouts.partials.js')
@yield('js')

</body>

</html>