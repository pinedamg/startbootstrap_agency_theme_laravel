const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    // Styles Files

    // Main Style file
    mix.less(['agency.less']);
    // Style Libraries
    mix.copy('./bower_components/bootstrap/dist/css/bootstrap.css', './resources/assets/css/bootstrap.css');
    mix.copy('./bower_components/font-awesome/css/font-awesome.css', './resources/assets/css/font-awesome.css');
    mix.copy('./bower_components/font-awesome/fonts', './public/fonts');
    mix.styles([
        'bootstrap.css',
        'font-awesome.css',
    ]);
    // JavaScript Libraries
    mix.copy('./bower_components/bootstrap/dist/js/bootstrap.js', './resources/assets/js/bootstrap.js');
    mix.copy('./bower_components/jquery/dist/jquery.js', './resources/assets/js/jquery.js');
    mix.copy('./bower_components/jquery.easing/js/jquery.easing.js', './resources/assets/js/jquery.easing.js');
    mix.copy('./bower_components/jqBootstrapValidation/dist/jqBootstrapValidation-1.3.7.js', './resources/assets/js/jqBootstrapValidation.js');
    mix.scripts([
        'jquery.js',
        'bootstrap.js',
        'jqBootstrapValidation.js',
        'jquery.easing.js',
        'agency.js',
        'contact_me.js'
    ]);
});